# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository is the master repository for a project called Trail2Kafka.

The aim of this project is to pull data from an infinite source(typically tailing file), 
and push it into kafka in realtime in a fault tolerant manner.

Since this project was targated with a use case that cropped up in a Stock market scenario, so its capable of dealing with a
torrent of records(~10k records/sec) pushed into the file. However, the program can be used in any other use case as well.


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact